import datetime
# importing the Blueprint class and the render_template function from the Flask 
from flask import Blueprint, render_template



# Blueprint is used to define sets of routes and views
# its an object of Blueprint that takes args first name of the Blueprint that is main 
main = Blueprint('main', __name__)


# Function to generate random image URL
def get_random_image_url():
    return "https://i.pravatar.cc/300"


# Function to add ordinal indicators to the day part of the date
def ordinal_indicator(day):
    if 10 <= day % 100 <= 20:
        suffix = "th"
    else:
        suffix = {1: "st", 2: "nd", 3: "rd"}.get(day % 10, "th")
    return  suffix

@main.route("/")
def index():
    
        today_date = datetime.date.today()
        
        suffix = ordinal_indicator(today_date.day)
        image_url="https://i.pravatar.cc/300"
        new_image_url = get_random_image_url()
        return render_template('index.html', suffix=suffix,image_url=new_image_url,day=today_date.day, month=today_date.strftime("%b"), year=today_date.year)
    
