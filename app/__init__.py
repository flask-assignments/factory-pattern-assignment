# inside this app package will  initialize anything which specific to the app package.
from flask import Flask


def create_app():
    app = Flask(__name__)
    
    
    # By registering the blueprint, you're telling Flask to include the routes and views defined
    # in the main blueprint as part of the application
    from app.routes import main
    app.register_blueprint(main)

    return app

